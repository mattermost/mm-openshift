#!/bin/bash
config=/mattermost/config/config.json
echo -ne "Configure database connection..."
if [ ! -f $config ]
then
    cp /config.template.json $config
#    PGUSER=$(</etc/pgsecrets/username)
#    PGPASS=$(</etc/pgsecrets/password)

    # SITE_URL required from MM 3.8 - use @ as escape char as variables contain / in Urls
    sed -i -e "s@SITE_URL@$SITE_URL@g" $config

    sed -i -e "s/POSTGRES_USER/$PGUSER/g" $config
    sed -i -e "s/POSTGRES_PASSWORD/$PGPASS/g" $config
    sed -i -e "s/POSTGRES_SERVICE_HOST/$POSTGRES_SERVICE_HOST/g" $config
    sed -i -e "s/POSTGRES_SERVICE_PORT/$POSTGRES_SERVICE_PORT/g" $config
    sed -i -e "s/POSTGRES_DATABASE_NAME/$POSTGRES_DATABASE_NAME/g" $config
    sed -i -e "s/OAUTH_SECRET/$OAUTH_SECRET/g" $config
    sed -i -e "s/OAUTH_ID/$OAUTH_ID/g" $config
    # use @ as escape char as variables contain / in Urls
    sed -i -e "s@OAUTH_AUTHORIZE@$OAUTH_AUTHORIZE@g" $config
    sed -i -e "s@OAUTH_TOKEN@$OAUTH_TOKEN@g" $config
    sed -i -e "s@OAUTH_API@$OAUTH_API@g" $config

    sed -i -e "s/LOG_LEVEL/$LOG_LEVEL/g" $config

    sed -i -e "s/AT_REST_ENCRYPT_KEY/$AT_REST_ENCRYPT_KEY/g" $config
    sed -i -e "s/PUBLIC_LINK_SALT/$PUBLIC_LINK_SALT/g" $config
    sed -i -e "s/INVITE_SALT/$INVITE_SALT/g" $config
    sed -i -e "s/PASSWORD_RESET_SALT/$PASSWORD_RESET_SALT/g" $config

    # WebRTC Gateway settings - use @ as escape char as variables contain / in Urls
    sed -i -e "s@WEBRTC_GATEWAYSOCKETURL@$WEBRTC_GATEWAYSOCKETURL@g" $config
    sed -i -e "s@WEBRTC_GATEWAYADMINURL@$WEBRTC_GATEWAYADMINURL@g" $config
    sed -i -e "s/WEBRTC_GATEWAYADMINSECRET/$WEBRTC_GATEWAYADMINSECRET/g" $config

    # Replace gitlab button
    sed -i -e 's/defaultMessage:"GitLab"/defaultMessage:"Login"/g' /mattermost/client/*.js
    sed -i -e 's/defaultMessage:"GitLab Single Sign-On"/defaultMessage:"CERN Single Sign-On"/g' /mattermost/client/*.js
    sed -i -e 's/"login.gitlab":"GitLab"/"login.gitlab":"Login"/g' /mattermost/client/*.js
    sed -i -e 's/"signup.gitlab":"GitLab Single Sign-On"/"signup.gitlab":"CERN Single Sign-On"/g' /mattermost/client/*.js
    sed -i -e 's/"login.gitlab": "GitLab"/"login.gitlab": "Login"/g' /mattermost/client/i18n/*.json
    sed -i -e 's/"signup.gitlab": "GitLab Single Sign-On"/"signup.gitlab": "CERN Single Sign-On"/g' /mattermost/client/i18n/*.json

# useless
#    chmod a+w $config
    echo OK
else
    echo SKIP
fi

echo "Starting platform"
cd /mattermost/bin
./platform
