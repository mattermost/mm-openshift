# Mattermost version update
- Update download path to the latest version from https://about.mattermost.com/download/ (usually updating `MM_VERSION` is enough)
- Update docker-entry.sh to replace the sed string replacements according to the new files. This replaces Gitlab word by CERN Login on authentication page. In most cases the existing commands work fine; test with the dev instance (`Dev` branch) before deploying to prod!

# Redeploy
- https://openshift.cern.ch : click builds, select current build, click start build
- Can also be done via `oc` command-line client: `oc start-build mm-openshift` (after logging in and selecting the project)
- dev uses a 'recreate' deployment (i.e. with downtime), but the production service does a rolling update so there is no real noticable downtime

# Configuration
- the config file is not stored in a persistent volume; any changes made in the admin console are lost in a redeploy
- edit `config.template.json` in this repo instead; this will be included in a new build/deploy

# WebRTC enabling
**Needs Janus Gateway, setup steps below:**


# CC7 Deployment of Janus Gateway
## VM mmwebrtc.cern.ch

Autoenroll for certificate: http://cern.ch/ca , enable CERN Host certificate

On mmwebrtc:
`
/usr/bin/yum install cern-get-certificate
/usr/sbin/cern-get-certificate --autoenroll
/usr/sbin/cern-get-certificate --status
`

Certificates installed in:
  - private key file: `/etc/pki/tls/private/mmwebrtc.cern.ch.key`
  - cert PEM file:    `/etc/pki/tls/certs/mmwebrtc.cern.ch.pem`
  - cert DER file:    `/etc/pki/tls/certs/mmwebrtc.cern.ch.crt`

## Deploy Janus: https://github.com/meetecho/janus-gateway

`
yum -y install libmicrohttpd-devel jansson-devel libnice-devel \
   openssl-devel libsrtp-devel sofia-sip-devel glib-devel \
   opus-devel libogg-devel libcurl-devel pkgconfig gengetopt \
   libtool autoconf automake
yum -y install wget git cmake
yum -y install epel-release
`

 ==> Missing sofia-sip-devel: to be fixed

### LibWebSockets install
`cd ~`
git clone git://git.libwebsockets.org/libwebsockets # Fails
`git clone https://github.com/warmcat/libwebsockets.git
cd libwebsockets`
If you want the stable version of libwebsockets, uncomment the next line
//git checkout v1.5-chrome47-firefox41
`mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_C_FLAGS="-fpic" ..
make && sudo make install`

Reload libraries
`ldconfig`

## Janus compile and install
`
cd ~
git clone https://github.com/meetecho/janus-gateway.git
cd janus-gateway
sh autogen.sh
./configure --prefix=/opt/janus
make
make install
`

If not yet configured
`make configs`

Set config files
`cp /afs/cern.ch/user/o/ormancey/janus/* /opt/janus/etc/janus/`
admin_secret = cernhiggsboson 

Configure Certificates
 cert_pem = /opt/janus/certs/certificate.crt
 cert_key = /opt/janus/certs/privateKey.key
 link them to autoenrol certs and set permissions
`mkdir /opt/janus/certs
ln -s /etc/pki/tls/certs/mmwebrtc.cern.ch.pem /opt/janus/certs/certificate.crt
ln -s /etc/pki/tls/private/mmwebrtc.cern.ch.key /opt/janus/certs/privateKey.key`

## Open Firewall Ports
`
firewall-cmd --permanent --add-port=8189/tcp
firewall-cmd --permanent --add-port=8188/tcp
firewall-cmd --permanent --add-port=7089/tcp
firewall-cmd --permanent --add-port=7088/tcp
firewall-cmd --reload
`

## Start service
`/opt/janus/bin/janus`

Set systemctl to start and restart service
`cp /afs/cern.ch/user/o/ormancey/janus/janus.service /usr/lib/systemd/system/`
or 
add to file /usr/lib/systemd/system/janus.service :

`
[Unit]
Description=Janus Webrtc Gateway
After=network.target

[Service]
Type=simple
PIDFile=/var/run/janus.pid
ExecStart=/opt/janus/bin/janus
ExecReload=/usr/bin/kill -HUP '/bin/cat /var/run/janus.pid'
ExecStop=/usr/bin/kill -9 '/bin/cat /var/run/janus.pid'
KillMode=process
Restart=on-failure
RestartSec=30

[Install]
WantedBy=multi-user.target
`

Then configure systemctl
`systemctl preset janus.service`
Test stuff
`systemctl start janus.service
systemctl stop janus.service
systemctl daemon-reload # if you change XXX.service`
enable service start at boot
`systemctl enable janus.service`
Verify
`systemctl is-enabled janus.service`

Test if all good on websocket
`openssl s_client -connect mmwebrtc.cern.ch:8189`


