FROM ubuntu:14.04

RUN apt-get update && apt-get -y upgrade && apt-get -y install wget curl netcat

ENV MM_VERSION 4.10.1

RUN mkdir -p /mattermost/data \
	&& curl -L https://releases.mattermost.com/$MM_VERSION/mattermost-team-$MM_VERSION-linux-amd64.tar.gz -o mattermost.tar.gz \
	&& tar -xvzf mattermost.tar.gz && rm mattermost.tar.gz \
	&& chmod -R a+w /mattermost

RUN curl -L https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt -o /usr/local/share/ca-certificates/cernroot2.crt \
    && curl -L https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority.crt -o /usr/local/share/ca-certificates/cerngrid.crt \
    && curl -L https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority.crt -o /usr/local/share/ca-certificates/cerncert.crt \
    && update-ca-certificates 

COPY config.template.json /
COPY docker-entry.sh /

RUN chmod +x /docker-entry.sh \
    && rm /mattermost/config/config.json

ENTRYPOINT /docker-entry.sh

EXPOSE 8080
