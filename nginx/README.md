This is a small nginx image to redirect Mattermost's /login to the gitlab login page.

How to deploy:

# create nginx image build configuration
oc new-app https://gitlab.cern.ch/mattermost/mm-openshift.git --context-dir='nginx' --name='nginx'
# to rebuild the image after changes in configuration
oc start-build bc/nginx
# create a route to tell OPenshift that /login should be handled by nginx instead of Mattermost
oc create route edge --path='/login' --service='nginx'